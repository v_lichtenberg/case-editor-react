const data = {
    caseData: [],
    checkClientExistence: {
        entityNumber: 108793462499
    }
};

// ### is a replacement to anonymise data.
for (let i = 0; i < 100; i++) {
    const obj = {
        "entityNumber": `${i+108793462499}`,
        "BPID": `${i}`,
        "accountNumber": `${i}`,
        "customerNumber": null,
        "SAPAccountNumber": `${i}`,
        "caseID": "637",
        "migrationCandidate": "Y",
        "migrationEligibility": "Y",
        "###Status": null,
        "VodsDateTime": "1800-01-01T00:00:00.000Z",
        "currentCardBrand": "BLUE_BZL",
        "creditLimit": 15000,
        "debitInterestRate": 20,
        "creditInterestRate": 5,
        "emailAddressInSync": "AWC@BPW.CO.ZA",
        "accountOpenDate": "2004-12-20T00:00:00.000Z",
        "emailAddressGroupAddress": "",
        "grossMonthlyIncome": 500000,
        "incomeConfidenceLevel": "L",
        "creditStatusReason": "ZCONT_MIGR",
        "creditlimitRule": "AM_UPT_THK",
        "creditRating": "740",
        "creditRiskClass": "4",
        "debitInterestRateC001": 10.5,
        "debitInterestRateP001": 10.5,
        "debitInterestRateC002": 8.25,
        "debitInterestRateP002": 8.25,
        "debitInterestRateC003": 6,
        "debitInterestRateP003": 4,
        "debitInterestRateP004": 4,
        "debitInterestRateC005": 3.75,
        "debitInterestRateP016": 3.5,
        "debitOrderBank": null,
        "debitOrderBranch": "253305",
        "debitOrderBankAccount": "100101101",
        "debitOrderBankAccountType": "Current",
        "debitOrderOption": "5",
        "debitOrderFixedPaymentAmount": 1000,
        "addressLine1": "",
        "addressLine2": "",
        "suburb": null,
        "city": "",
        "state": "",
        "postalCode": "",
        "billingDay": 0,
        "AccountHolderName": null,
        "BalanceTransferAuthCode": null,
        "GetNextPriority": null,
        "BOAStatus": 1,
        "maritalStatus": null,
        "maritalStatusSAP": null,
        "maritalType": null,
        "maritalTypeSAP": null,
        "occupation": null,
        "occupationSAP": null,
        "employer": null,
        "title": null,
        "titleSAP": null,
        "language": null,
        "countryOfBirth": null,
        "GetNextPriorityDate": "1800-01-01T00:00:00.000Z"
    };
    data.caseData.push(obj)
}

const jsonServer = require('json-server');
const server = jsonServer.create();
const middlewares = jsonServer.defaults();

// Set default middlewares (logger, static, cors and no-cache)
server.use(middlewares);

// Add custom routes before JSON Server router
server.get('/caseData', (req, res) => {
    if (req.query && req.query.entityNumber) {
        res.jsonp(data.caseData.filter((e) => e.entityNumber === req.query.entityNumber)[0]);
    } else {
        res.jsonp(data.caseData)
    }
});

// To handle POST, PUT and PATCH you need to use a body-parser
// You can use the one used by JSON Server
server.use(jsonServer.bodyParser);
server.use((req, res, next) => {
    if (req.method === 'POST') {
        req.body.createdAt = Date.now()
    }
    if (req.method === 'PUT') {
        req.body.createdAt = Date.now()
    }
    // Continue to JSON Server router
    next()
});

server.post('/checkClientExistence', (req, res, next) => {
    req.method = 'GET';
    req.query = req.body;
    res.jsonp(data.checkClientExistence);
});

server.put('/caseData', (req, res) => {
    req.method = 'GET';
    req.query = req.body;
    if (req.query && req.query.entityNumber) {
        //persist data
        var record = data.caseData.filter((e) => e.entityNumber === req.query.entityNumber)[0];
        record = Object.assign(record, req.query);
        //respond
        res.jsonp(record);
    }
});

server.listen(3001, () => {
    console.log('JSON Server is running')
});
