import React from 'react';
import TestRenderer from 'react-test-renderer';
import App from './App';

const renderer: any = TestRenderer.create(<App />);
const root: any = renderer.root;

it('is sane', () => {
  expect(true).toBe(true);
});

it('renders some expected elements', () => {
  expect(root.findByProps({className: 'App container'})).toBeDefined();
  // expect one form to be rendered
  expect(root.findAllByType('form').length).toBe(1);
});

