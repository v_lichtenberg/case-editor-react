import React, {useState} from 'react';
import useCaseService from "../services/CaseService";
import checkClientExistenceService, {EntityNumber} from '../services/checkClientExistenceService';
import environmentService from "../services/Environment";
import {Case} from "../types/Case";

export interface EntityResult {
  entityNumber: number;
}

const { networkIP, mockServerPort, apiUri, caseDataEndpointName } = environmentService();

const IdInput = (props: { viewCase: (record: Case | null, view: boolean) => void }) => {
  const [identifierInput, setIdentifierInput] = useState<string>('');
  const isIdNumber = identifierInput.length === 13;
  const {getCaseByEntityNumber} = useCaseService();

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    event.persist();
    setIdentifierInput(event.target.value);
  };

  const handleFormSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (!identifierInput) {
      console.error('Invalid input');
      return false;
    }

    let entityNumber: number;

    const getCase = (entityNumber: number) => {
      getCaseByEntityNumber(`http://${networkIP}:${mockServerPort}${apiUri}/${caseDataEndpointName}`, entityNumber.toString())
        .then((res: Case) => {
          console.info(`Found client with case number ${entityNumber}`);
          props.viewCase(
            res,
            true
          )
        })
        .catch(() => {
          console.error(`Could not find client data with ID ${entityNumber}`);
          props.viewCase(
            null,
            false
          )
        });
    };

    if (isIdNumber) {
      checkClientExistenceService('ZA', identifierInput, 'ZSAIDD')
        .then((res: EntityNumber) => {
          console.info(`Recognised South African ID number ${identifierInput}. Searching...`);
          return res.entityNumber;
        })
        .then(res => getCase(res))
        .catch((err) => console.error(err));
    } else {
      entityNumber = parseInt(identifierInput);
      getCase(entityNumber);
      console.info(`Searching for entity number ${entityNumber}...`);
    }

  };

  return (
    <div className="row mt-5">
      <div className="col">
        <form onSubmit={handleFormSubmit}>
          <input
            className="form-control"
            type="text"
            name="name"
            value={identifierInput}
            onChange={handleChange}
            placeholder="Enter an entity number or South African ID number"
          />
        </form>
      </div>
    </div>
  );
};

export default IdInput;