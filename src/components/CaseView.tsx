import React from 'react';
import {Case} from "../types/Case";

const CaseView = (props: { currentCase: Partial<Case> }) => {

  return (<div>
      <h2>Case {props.currentCase.entityNumber}</h2>
      <pre className="caseview">
        {JSON.stringify(props.currentCase, null, 2)}
      </pre>
    </div>
  )
};

export default CaseView;