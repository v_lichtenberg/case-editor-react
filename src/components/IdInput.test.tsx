import React from 'react';
import TestRenderer from 'react-test-renderer';
import IdInput from './IdInput';

const props: any = {
  viewCase() {
    return;
  }
};
const renderer: any = TestRenderer.create(<IdInput {...props}/>);
const root: any = renderer.root;

it('Sanity check', () => {
  expect(true).toBe(true);
});

it('Actual test', () => {
  // expect only one child to be in the form, and its type to be input
  const formEl = root.findByType('form');
  expect(formEl).toBeDefined();
  expect(formEl.children[0].type).toBe('input');
  const inputEl = root.findByType('input');
  expect(inputEl.props.placeholder).toBe('Enter an entity number or South African ID number');
});
