import React from 'react';
import {Case} from '../types/Case';

const CaseTable = (props: { cases: Case[], viewCase: (record: Case, view: boolean) => void, currentCase: Case | null }) => {

  return (<table className={'table table-hover'}>
      <thead>
      <tr>
        <th>entityNumber</th>
        <th>migrationEligibility</th>
        <th>migrationCandidate</th>
        <th>currentCardBrand</th>
      </tr>
      </thead>
      <tbody>
      {(props.cases.length > 0) ?
        props.cases.map((item: Case) => (
            <tr
              key={item.entityNumber}
              onClick={(el) => {
                console.log(el);
                props.viewCase(
                  item,
                  true
                )
              }}
              className={props.currentCase && item.entityNumber === props.currentCase.entityNumber ? 'table-active' : ''}>
              <td>{item.entityNumber}</td>
              <td>{item.migrationEligibility}</td>
              <td>{item.migrationCandidate}</td>
              <td>{item.currentCardBrand}</td>
            </tr>
          )
        ) : (
          <tr>
            <td colSpan={4}>No users</td>
          </tr>
        )
      }
      </tbody>
    </table>
  )
};

export default CaseTable;