import React from 'react';
import TestRenderer from 'react-test-renderer';
import CaseTable from './CaseTable';

const props: any = {
  cases: [],
  currentCase: {},
  viewCase() {
    return;
  }
};
let renderer: any = TestRenderer.create(<CaseTable {...props}/>);
let root: any = renderer.root;

it('Sanity check', () => {
  expect(true).toBe(true);
});

it('Actual test', () => {

  const casesMock = [
    {"entityNumber": "108793462499", "BPID": "0", "accountNumber": "0", "customerNumber": null, "SAPAccountNumber": "0", "caseID": "637", "migrationCandidate": "Y", "migrationEligibility": "Y", "###Status": null, "VodsDateTime": "1800-01-01T00:00:00.000Z", "currentCardBrand": "BLUE_BZL", "creditLimit": 15000, "debitInterestRate": 20, "creditInterestRate": 5, "emailAddressInSync": "AWC@BPW.CO.ZA", "accountOpenDate": "2004-12-20T00:00:00.000Z", "emailAddressGroupAddress": "", "grossMonthlyIncome": 500000, "incomeConfidenceLevel": "L", "creditStatusReason": "ZCONT_MIGR", "creditlimitRule": "AM_UPT_THK", "creditRating": "740", "creditRiskClass": "4", "debitInterestRateC001": 10.5, "debitInterestRateP001": 10.5, "debitInterestRateC002": 8.25, "debitInterestRateP002": 8.25, "debitInterestRateC003": 6, "debitInterestRateP003": 4, "debitInterestRateP004": 4, "debitInterestRateC005": 3.75, "debitInterestRateP016": 3.5, "debitOrderBank": null, "debitOrderBranch": "253305", "debitOrderBankAccount": "100101101", "debitOrderBankAccountType": "Current", "debitOrderOption": "5", "debitOrderFixedPaymentAmount": 1000, "addressLine1": "", "addressLine2": "", "suburb": null, "city": "", "state": "", "postalCode": "", "billingDay": 0, "AccountHolderName": null, "BalanceTransferAuthCode": null, "GetNextPriority": null, "BOAStatus": 1, "maritalStatus": null, "maritalStatusSAP": null, "maritalType": null, "maritalTypeSAP": null, "occupation": null, "occupationSAP": null, "employer": null, "title": null, "titleSAP": null, "language": null, "countryOfBirth": null, "GetNextPriorityDate": "1800-01-01T00:00:00.000Z" },
    {"entityNumber": "108793462500", "BPID": "0", "accountNumber": "0", "customerNumber": null, "SAPAccountNumber": "0", "caseID": "637", "migrationCandidate": "Y", "migrationEligibility": "Y", "###Status": null, "VodsDateTime": "1800-01-01T00:00:00.000Z", "currentCardBrand": "BLUE_BZL", "creditLimit": 15000, "debitInterestRate": 20, "creditInterestRate": 5, "emailAddressInSync": "AWC@BPW.CO.ZA", "accountOpenDate": "2004-12-20T00:00:00.000Z", "emailAddressGroupAddress": "", "grossMonthlyIncome": 500000, "incomeConfidenceLevel": "L", "creditStatusReason": "ZCONT_MIGR", "creditlimitRule": "AM_UPT_THK", "creditRating": "740", "creditRiskClass": "4", "debitInterestRateC001": 10.5, "debitInterestRateP001": 10.5, "debitInterestRateC002": 8.25, "debitInterestRateP002": 8.25, "debitInterestRateC003": 6, "debitInterestRateP003": 4, "debitInterestRateP004": 4, "debitInterestRateC005": 3.75, "debitInterestRateP016": 3.5, "debitOrderBank": null, "debitOrderBranch": "253305", "debitOrderBankAccount": "100101101", "debitOrderBankAccountType": "Current", "debitOrderOption": "5", "debitOrderFixedPaymentAmount": 1000, "addressLine1": "", "addressLine2": "", "suburb": null, "city": "", "state": "", "postalCode": "", "billingDay": 0, "AccountHolderName": null, "BalanceTransferAuthCode": null, "GetNextPriority": null, "BOAStatus": 1, "maritalStatus": null, "maritalStatusSAP": null, "maritalType": null, "maritalTypeSAP": null, "occupation": null, "occupationSAP": null, "employer": null, "title": null, "titleSAP": null, "language": null, "countryOfBirth": null, "GetNextPriorityDate": "1800-01-01T00:00:00.000Z" }
  ];
  props.cases = casesMock;

  // recreate renderer with mock
  const renderer: any = TestRenderer.create(<CaseTable {...props}/>);
  const root: any = renderer.root;
  const theadEl = root.findByType('thead');
  const tbodyEl = root.findByType('tbody');

  expect(theadEl).toBeDefined();
  expect(theadEl.children.length).toBe(1);
  expect(tbodyEl).toBeDefined();
  expect(tbodyEl.children.length).toBe(2);

  const firstRowEl = tbodyEl.children[0].props.onClick();
  console.log(renderer.toJSON().children[1].children[0]);
});
