import React, {useState, useEffect} from 'react';
import editCaseService from "../services/editCaseService";
import {Case, YesNo, CardBrand} from "../types/Case";

interface FormModel {
  migrationEligibility: YesNo,
  migrationCandidate: YesNo,
  currentCardBrand: CardBrand
}

const CaseEdit = (props: {
  currentCase: Case,
  updateCurrentCase: (record: Case) => void,
  viewEdit: (view: boolean) => void,
  updateCaseTable: (cases: Case[]) => void
}) => {

  const cardBrands = ["GOLD", "BLACK", "PURPLE", "BLUE_BZL", "PLATINUM"];

  const [formValues, setFormValues] = useState<FormModel>({
    migrationEligibility: props.currentCase.migrationEligibility,
    migrationCandidate: props.currentCase.migrationCandidate,
    currentCardBrand: props.currentCase.currentCardBrand
  });

  useEffect(() => {
    setFormValues({
      migrationEligibility: props.currentCase.migrationEligibility,
      migrationCandidate: props.currentCase.migrationCandidate,
      currentCardBrand: props.currentCase.currentCardBrand
    });
  },[props.currentCase]);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    event.persist();
    setFormValues((formValues: FormModel) => ({
      ...formValues,
      [event.target.name]: event.target.value
    }));
  };

  const saveChanges = () => {

    if (props.currentCase.entityNumber) {
      editCaseService(
        props.currentCase.entityNumber,
        formValues.migrationEligibility,
        formValues.migrationCandidate,
        formValues.currentCardBrand
      )
        .then((res: Case) => {
          props.updateCurrentCase(res);
          props.updateCaseTable([res]);
        });
    }
  };

  const handleFormSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    saveChanges();
    props.viewEdit(false);
  };

  const closeForm = () => {
    props.viewEdit(false);
  };

  return (
    <div>
      <div className="row">
        <div className="col">
          <h2>Case {props.currentCase.entityNumber}</h2>
          <form onSubmit={handleFormSubmit}>
            <label>migrationEligibility</label>
            <div className="form-group">
              <div className="form-check">
                <input className="form-check-input" onChange={handleChange} type="radio"
                       name="migrationEligibility" id="migrationEligibility1" value="Y"
                       checked={(formValues.migrationEligibility === 'Y')}/>
                <label className="form-check-label" htmlFor="migrationEligibility1">Yes</label>
              </div>
              <div className="form-check">
                <input className="form-check-input" onChange={handleChange} type="radio"
                       name="migrationEligibility" id="migrationEligibility2" value="N"
                       checked={(formValues.migrationEligibility === 'N')}/>
                <label className="form-check-label" htmlFor="migrationEligibility2">No</label>
              </div>
            </div>
            <label>migrationCandidate</label>
            <div className="form-group">
              <div className="form-check">
                <input className="form-check-input" onChange={handleChange} type="radio" name="migrationCandidate"
                       id="migrationCandidate1"
                       value="Y"
                       checked={(formValues.migrationCandidate === 'Y')}/>
                <label className="form-check-label" htmlFor="migrationCandidate1">Yes</label>
              </div>
              <div className="form-check">
                <input className="form-check-input" onChange={handleChange} type="radio" name="migrationCandidate"
                       id="migrationCandidate2"
                       value="N"
                       checked={(formValues.migrationCandidate === 'N')}/>
                <label className="form-check-label" htmlFor="migrationCandidate2">No</label>
              </div>
            </div>
            <label>currentCardBrand</label>
            <div className="form-group">
              {cardBrands.map((item: string, i: number) => {
                return <div className="form-check" key={i}>
                  <input className="form-check-input" onChange={handleChange} type="radio" name="currentCardBrand"
                         id={'currentCardBrand' + i}
                         value={item}
                         checked={(formValues.currentCardBrand === item)}/>
                  <label className="form-check-label" htmlFor={'currentCardBrand' + i}>{item}</label>
                </div>
              })}
            </div>
            <button type="submit" className="btn btn-primary" value="Submit">Save & exit</button>
            <button type="button" className="btn btn-secondary" value="Save" onClick={saveChanges}>Save</button>
            <button type="button" className="btn btn-light" value="Cancel" onClick={closeForm}>Cancel</button>
          </form>
        </div>
        <div className="col">
          <h2>Form model</h2>
          <pre className="caseview">
            {JSON.stringify(formValues, null, 2)}
          </pre>
        </div>
      </div>
    </div>
  );
};

export default CaseEdit;