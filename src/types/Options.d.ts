export type YesNo<string> =
  | "Y"
  | "N";