import React, {useState} from 'react';
import useCasesService from './services/useCasesService';
import IdInput from './components/IdInput';
import CaseTable from './components/CaseTable';
import CaseView from "./components/CaseView";
import CaseEdit from "./components/CaseEdit";
import {Case} from "./types/Case";

import './App.css';

const App: React.FC = () => {

  const initialCaseState = null as unknown as Case;
  const [currentCase, setCurrentCase] = useState<Case | null>(initialCaseState);
  const initialAllCasesState = [] as Case[];
  const [allCases, setAllCases] = useState<Case[]>(initialAllCasesState);
  const [viewing, setViewing] = useState(false);
  const [editing, setEditing] = useState(false);

  const useCasesByUrlServiceResult = useCasesService(allCases);

  const viewCase = (record: Case | null, view: boolean) => {
    setCurrentCase(record);
    setViewing(view);
    setEditing(view);
  };

  const viewEdit = (view: boolean) => {
    setEditing(view);
  };

  const updateCurrentCase = (record: Case) => {
    setCurrentCase(record);
  };

  const updateCaseTable = (cases: Case[]) => {
    setAllCases(cases);
  };

  return (
    <div className="App container">
      <IdInput viewCase={viewCase}/>
      <div className="row my-5">
        {(editing && currentCase) &&
        <div className="col">
            <CaseEdit currentCase={currentCase} updateCurrentCase={updateCurrentCase} viewEdit={viewEdit} updateCaseTable={updateCaseTable}/>
        </div>}
      </div>
      <div className="row">
        <div className="col">
          {useCasesByUrlServiceResult.status === 'loading' && <div>Loading...</div>}
          {useCasesByUrlServiceResult.status === 'loaded' && <h2>All cases</h2>}
          {useCasesByUrlServiceResult.status === 'loaded' &&
          <CaseTable cases={useCasesByUrlServiceResult.payload} viewCase={viewCase}
                     currentCase={currentCase}/>}
          {useCasesByUrlServiceResult.status === 'error' && <div>Error message</div>}
        </div>
        {(viewing && currentCase) && <div className="col"><CaseView currentCase={currentCase}/></div>}
      </div>
    </div>
  );
};

export default App;
