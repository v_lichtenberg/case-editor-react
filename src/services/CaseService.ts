import {useState} from 'react';
import {Service} from '../types/Service';
import {Case} from '../types/Case';

const useCaseService = () => {
  const [service, setService] = useState<Service<Case[]>>({
    status: 'init'
  });

  const getCaseByEntityNumber = (url: string, entityNumber: string) => {
    setService({status: 'loading'});

    const fetchLocation = `${url}?entityNumber=${entityNumber}`;

    return new Promise<Case>((resolve, reject) => {
      fetch(fetchLocation)
        .then(response => response.json())
        .then(response => {
          setService({status: 'loaded', payload: response});
          resolve(response);
        })
        .catch(error => {
          setService({status: 'error', error});
          reject(error);
        });
    });
  };

  return {
    service,
    getCaseByEntityNumber
  };
};

export default useCaseService;