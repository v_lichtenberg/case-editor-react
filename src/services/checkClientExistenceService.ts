import environmentService from "./Environment";

const { networkIP, mockServerPort, apiUri } = environmentService();

export interface EntityNumber {
  entityNumber: number;
}

const checkClientExistenceService = (idCountryOfIssue:string, idNumber:string, idType:string) => {

  return new Promise<EntityNumber>((resolve, reject) => {
    const query = {idCountryOfIssue:idCountryOfIssue, idNumber:idNumber, idType:idType};

    fetch(`http://${networkIP}:${mockServerPort}${apiUri}/checkClientExistence`, {
      method: 'POST',
      body: JSON.stringify(query)
    })
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export default checkClientExistenceService;