const environmentService = () => {

  const apiUri: string = '',
    caseDataEndpointName: string = 'caseData',
    mockServerPort: string = '3001',
    networkIP: string = 'localhost';

  return {
    apiUri,
    caseDataEndpointName,
    mockServerPort,
    networkIP
  };
};

export default environmentService;