import environmentService from "./Environment";
import {Case} from "../types/Case";

const { networkIP, mockServerPort, apiUri, caseDataEndpointName } = environmentService();

const editCaseService = (entityNumber: string, migrationEligibility: string, migrationCandidate: string, currentCardBrand: string) => {

  const query = {
    entityNumber: entityNumber,
    migrationEligibility: migrationEligibility,
    migrationCandidate: migrationCandidate,
    currentCardBrand: currentCardBrand
  };

  return new Promise<Case>((resolve, reject) => {
    fetch(`http://${networkIP}:${mockServerPort}${apiUri}/${caseDataEndpointName}?entityNumber=${entityNumber}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(query)
    })
      .then(res => res.json())
      .then(res => resolve(res))
      .catch(err => reject(err));
  });

};

export default editCaseService;