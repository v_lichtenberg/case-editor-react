import {useEffect, useState} from 'react';
import environmentService from "./Environment";
import {Service} from '../types/Service';
import {Case} from '../types/Case';

const useCasesService = (cases: Case[]) => {
  const [result, setResult] = useState<Service<Case[]>>({
    status: 'loading'
  });

  useEffect(() => {
    const { networkIP, mockServerPort, apiUri, caseDataEndpointName } = environmentService();

    setResult({status: 'loading'});
    fetch(`http://${networkIP}:${mockServerPort}${apiUri}/${caseDataEndpointName}`)
      .then(response => response.json())
      .then(response => {
        setResult({status: 'loaded', payload: response});
      })
      .catch(error => setResult({status: 'error', error}));
  }, [cases]);

  return result;
};

export default useCasesService;